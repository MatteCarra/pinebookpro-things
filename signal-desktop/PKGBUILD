# Maintainer: Jean Lucas <jean@4ray.co>

pkgname=signal-desktop
_pkgname=Signal-Desktop
pkgver=5.39.0
pkgrel=2
pkgdesc="Signal Private Messenger for Linux"
arch=(aarch64 x86_64)
url=https://signal.org
license=(AGPL3)
replaces=('signal-desktop-bin')
depends=('gtk3' 'libvips' 'libxss' 'hicolor-icon-theme')
_nodeversion=16.13.0
makedepends=(yarn git git-lfs nodejs
             npm python nvm)
makedepends_aarch64=(phantomjs
                     icu60 # required by the old phantomjs build
                     rustup # cargo # required for the zkgroup-builds
                    )
provides=(signal)
replaces=(signal)
source=(
  $pkgname-$pkgver.tar.gz::https://github.com/signalapp/$_pkgname/archive/v$pkgver.tar.gz
  $pkgname.desktop
  "expire-from-source-date-epoch.patch"
)
source_aarch64=(
  #"signal-ringrtc-node::git+https://github.com/lsfxz/signal-ringrtc-node.git#commit=ef3290927470be3b3450090501f455b787254c9a"
  "libsignal-client::git+https://github.com/signalapp/libsignal-client.git#tag=v0.16.0"
  "better-sqlite3.patch::https://gitlab.com/undef1/signal-desktop-builder/-/raw/master/patches/better-sqlite3.patch"
  "no_deb.patch"
)
# can't use it directly bc git-lfs causes trouble – files will remain lfs pointers
# "better-sqlite3::git+https://github.com/signalapp/better-sqlite3.git"
sha512sums=('f7dc8872f34ef27206ab0387a46c731e639c0c31223aa091b51e0244daf0fe8e10fa53267a03557dd2f4d33ff4cebfa318b31f624c10b6541d8df5c749334c77'
            '90cfee563a985bc73c4e7984715d190ae0b5c7aa887a7dc15c665980ca5cc8420b02f6c7a54e032c29e18876d5d51cfbe5027a9f0a59de3903f50fd469d73ce0'
            '1154859e87d8a2d649bc23210f2dd8aa473f268166559a51a2a64fe6ae094c101121535623b05b711bd87aab1f219627e9274fa542fdb0e5fe6f34b46fd7b7df')
sha512sums_aarch64=('SKIP'
                    #'SKIP'
                    'e485aaa8dcc7a78863a43ced74f3dfc4446bb844743ff4060b098325b01fa875f3cedfcaa87cd7a86afdb6b63f2af8e8eb36416dcc8677cf0ce22c9b9e13f5f4'
                    '53f7e785126fbbf8476173070d3f8d90a65e578d076caecc537e93f144003554f865cda6dfb3508a952e7aaebd8c503bb5aeb31cefce155c7d8700eb5ebbdc50')
prepare() {
  export npm_config_cache="$srcdir/npm_cache"
  _ensure_local_nvm
  nvm install ${_nodeversion} && nvm use ${_nodeversion}

  # git-lfs hook needs to be installed for one of the dependencies
  git lfs install

  cd ${srcdir}/$_pkgname-$pkgver

  if [[ $CARCH == 'aarch64' ]]; then
    #sed -r 's#("ringrtc": ").*"#\1file:../signal-ringrtc-node"#' -i package.json
    sed -r 's#("better-sqlite3": ").*"#\1file:../better-sqlite3"#' -i package.json

    if [ ! -d ${srcdir}/better-sqlite3 ]
    then
      git clone https://github.com/signalapp/better-sqlite3 ${srcdir}/better-sqlite3
    fi
    cd ${srcdir}/better-sqlite3
    git checkout b287153a5c6a5ac77f1e70df75d245e7a6e2286d

    # thanks @undef1 (gitlab) for this patch!
    patch -Np1 -i ${srcdir}/better-sqlite3.patch
  fi

  # NOTE: we might not need this anymore. let's try.
  # NOTE: well, of course we still do
  if [[ $CARCH == 'aarch64' ]]; then
    sed -r 's#("@signalapp/signal-client": ").*"#\1file:../libsignal-client/node"#' -i package.json
    cd ${srcdir}/libsignal-client/node

    yarn install
    yarn tsc
    mkdir -p prebuilds/linux-arm64
    mv ./build/Release/libsignal_client_linux_arm64.node prebuilds/linux-arm64/node.napi.node
  fi

  cd ${srcdir}/$_pkgname-$pkgver

  if [[ $CARCH == 'aarch64' ]]; then
    # this just drops the whole part where electron-builder tries to package a .deb release
    # ...which also includes multiple avenues of failure related to fpm, dependencies, tar, ar, xz etc.
    # basically, it's horrible – and luckily, not required at all for the Arch release
    patch -Np1 -i ../no_deb.patch
  fi

  # Allow higher Node versions
  # sed 's#"node": "#&>=#' -i package.json
  # Not used at the moment as we had to switch to nvm

  [[ $CARCH == "aarch64"  ]] && CFLAGS=`echo $CFLAGS | sed -e 's/-march=armv8-a//'` && CXXFLAGS="$CFLAGS"

  # We can't read the release date from git so we use SOURCE_DATE_EPOCH instead
  patch --forward --strip=1 --input="${srcdir}/expire-from-source-date-epoch.patch"

  yarn install --ignore-engines
}

build() {
  export npm_config_cache="$srcdir/npm_cache"
  _ensure_local_nvm
  nvm use ${_nodeversion}

  cd $_pkgname-$pkgver
  [[ $CARCH == "aarch64"  ]] && CFLAGS=`echo $CFLAGS | sed -e 's/-march=armv8-a//'` && CXXFLAGS="$CFLAGS"

  # yarn build-release doesn't like --arm64 / any switches directly

  yarn run-s --print-label generate build:typed-scss build:webpack # build:release build:zip

  if [[ $CARCH == 'aarch64' ]]; then
    SIGNAL_ENV=production yarn build:electron --arm64 --linux --dir --config.directories.output=release
  else
    SIGNAL_ENV=production yarn build:electron --x64 --linux --dir --config.directories.output=release
  fi
  yarn run-s build:release
}

package() {
  cd $_pkgname-$pkgver

  install -d "$pkgdir"/usr/{lib,bin}
  if [[ $CARCH == 'aarch64' ]]; then
    cp -a release/linux-arm64-unpacked "$pkgdir"/usr/lib/$pkgname
  else
    cp -a release/linux-unpacked "$pkgdir"/usr/lib/$pkgname
  fi

  ln -s "/usr/lib/${pkgname}/${pkgname}" "${pkgdir}/usr/bin/"

  chmod u+s "${pkgdir}/usr/lib/signal-desktop/chrome-sandbox"

  install -Dm 644 ../$pkgname.desktop -t "$pkgdir/usr/share/applications"
  for i in 16 24 32 48 64 128 256 512 1024; do
    install -Dm 644 build/icons/png/${i}x${i}.png \
      "$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png
  done
}

# https://wiki.archlinux.org/title/Node.js_package_guidelines#Using_nvm
_ensure_local_nvm() {
    # let's be sure we are starting clean
    which nvm >/dev/null 2>&1 && nvm deactivate && nvm unload
    export NVM_DIR="${srcdir}/.nvm"

    # The init script returns 3 if version specified
    # in ./.nvrc is not (yet) installed in $NVM_DIR
    # but nvm itself still gets loaded ok
    source /usr/share/nvm/init-nvm.sh || [[ $? != 1 ]]
}
