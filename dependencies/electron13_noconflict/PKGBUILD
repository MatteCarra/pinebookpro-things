# Maintainer: Dan Johansen <strit@manjaro.org>

pkgname=electron13-noconflict
_pkgname=electron13
pkgver=13.3.0
pkgrel=1
pkgdesc='Build cross platform desktop apps with web technologies - version: 13, non-conflicting'
arch=('aarch64')
url='https://electronjs.org/'
license=('MIT' 'custom')
depends=('c-ares' 'ffmpeg' 'gtk3' 'http-parser' 'libevent' 'libnghttp2'
         'libxslt' 'libxss' 'minizip' 'nss' 're2' 'snappy')
optdepends=('kde-cli-tools: file deletion support (kioclient5)'
            'trash-cli: file deletion support (trash-put)'
            "xdg-utils: open URLs with desktop's default (xdg-email, xdg-open)")
provides=("electron13")
conflicts=("electron13")
source=("https://github.com/electron/electron/releases/download/v${pkgver}/electron-v${pkgver}-linux-arm64.zip"
        "electron13.desktop")
md5sums=('e8bf4d53f04ee7c6f5b5f5f824279bb3'
         'b751abfb6c87ebba87a08f0b2aab9699')

package() {
  install -dm755 "${pkgdir}/usr/lib/${_pkgname}"
  cp -a "${srcdir}"/* "${pkgdir}/usr/lib/${_pkgname}"

  install -dm755 "${pkgdir}/usr/share/licenses/${_pkgname}"
  for l in "${pkgdir}/usr/lib/${_pkgname}"/{LICENSE,LICENSES.chromium.html}; do
    ln -s  \
      $(realpath --relative-to="${pkgdir}/usr/share/licenses/${_pkgname}" ${l}) \
      "${pkgdir}/usr/share/licenses/${_pkgname}"
  done

  install -dm755 "${pkgdir}"/usr/bin
  ln -s ../lib/${_pkgname}/electron "${pkgdir}"/usr/bin/${_pkgname}

  # Install .desktop and icon file (see default_app-icon.patch)
  install -Dm644 -t "${pkgdir}/usr/share/applications" ${_pkgname}.desktop
}
