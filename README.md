# PBP-Things

## What's this about?

Some odds and ends related to the Pinebook Pro, collected in one place to avoid just having them posted somewhere around the web.

PKGBUILDS are mostly taken from the upstream Arch / alarm / Manjaro repos or the AUR and modified to work with the PBP.

**BEWARE:** Most of this is probably not "clean" or "well done" but more about getting things working for me,
so be careful. It's not my fault if your machine breaks or your house catches on fire.

## I don't want to build things!

Some of the packages are provided with an Arch package repository as well: [privacyshark](https://privacyshark.zero-credibility.net/)

### Managed elsewhere, but might be useful

[Librewolf, a more privacy focused Firefox-(not-really-a-)fork](https://gitlab.com/librewolf-community/browser/arch), also in the AUR (-bin and regular flavor.)

[Caidao, a very slightly modified ungoogled-chromium](https://gitlab.com/ohfp/caidao). I just like to keep an unmodified chromium available next to an ungoogled one.

## briar{,-git}

briar-gtk (and dependencies). This is a "very alpha" release of briar for Linux based desktop / mobile systems.

## dependencies/electron{11,12}_noconflict

The upstream Manjaro electron11 and electron12 packages are somewhat messy.
They went half the way (it seems) to separate the electron binaries,
but then stopped after the binary and made everything conflict.

Those packages provide a way to have separate electron versions coexisting,
which allows you to run applications with electron >= 12 (providing native
Wayland support) while still being able to run applications depending on electron 11.

If even the installation of `electron11_noconflict` gives you (initial) conflicts,
you might have to run `pacman -S electron --assume-installed=electron11` first and
then run `pacman -S electron11_noconflict`. I hope to get a PR ready to address this
upstream soon, though.

## ferdi

Added back since it works again nowadays. Slightly modified from [the AUR](https://aur.archlinux.org/packages/ferdi-git/) to avoid having to build with ancient npm/nodejs versions.

## hamsket{,-nightly}-bin

Hamsket (open source continuation of Rambox) cannot be compiled on aarch64.

Fortunately, the binary can be extracted and directly run with electron. Based on [aur/hamsket-bin]( https://aur.archlinux.org/packages/hamsket-bin/ ).

Currently not continued anymore upstream. It seems the [dev is in a bad place](https://github.com/TheGoddessInari/hamsket/issues/258#issuecomment-778557482).

## hey-mail-bin

Adapted from [the AUR](https://aur.archlinux.org/packages/hey-mail-bin/). Throws away everything but the main application files and uses system electron.
This makes it much smaller, lets it run on `aarch64` as well, allows running on `linux-hardened` without
setting `kernel.unprivileged_userns_clone=1` and respects
flags in `~/.config/electron-flags.conf` – and thus can run natively on Wayland.

Patches a few things (dev environment checks) in the code so it's not provided as a binary until I'm
certain doing this and distributing it doesn't violate any license of theirs. It's easy to build
from the PKGBUILD, so that shouldn't be much of an issue.

## jitsi-meet-desktop

~~Modified from the AUR to build on aarch64, adding missing dependencies to cleanly build in a chroot and running via system electron~~.

I took over maintenance of the AUR package for jitsi-meet-desktop, so this just mirrors the AUR package as it's also provided in the binary repo.

## lrzip-git

lrzip is available in the repos. This version builds from the most recent git commit and is just a small fix to the AUR PKGBUILD to get it to build on `aarch64`.

## nordnm-git

This just mirrors/tracks the [AUR package](https://aur.archlinux.org/packages/nordnm-git).

## nheko

nheko can be compiled directly from the AUR, but takes a while (and with the default PKGBUILD the build process runs out of memory on the PBP), so it's available in the binary repo.

nheko-git currently crashes on aarch64, so stable is provided for now.

## rtl88x2bu-dkms-git

From [the AUR](https://aur.archlinux.org/packages/rtl88x2bu-dkms-git), just with a fix related to an arch/aarch64-related path.

## signal-desktop

Based on [upstream community/signal-desktop](https://www.archlinux.org/packages/community/x86_64/signal-desktop/).

Signal requires PhantomJS to be built. Building PhantomJS on arm/aarch64 is hell, so a prebuilt package is provided,
taken from [http://tardis.tiny-vps.com](http://tardis.tiny-vps.com/aarm/packages/p/phantomjs), which is an alarm package archive.


~~Needed a somewhat ugly workaround for sqlcipher to be able to apply the openssl-linking patch; basically on arm/aarch64,
most things get rebuilt / pulled in afresh so the patch gets overwritten again, when patching via PKGBUILD or with patch-package.~~

Uses better-sqlite3 now… which needs another backported / up-ported / whatever patch (pretty much the same fix that was required for sqlcipher since 1784).

Plus even more "weird workarounds" to have node-zkgroup pull in a ffi-napi that actually supports aarch64 and having to build zkgroup as well, as only an x86_64 `libzkgroup.so` is provided.
~~Seems like upstream does not really care too much about non-android arm/aarch64 support :/~~

Things are improving. Cross compilation of arm64 binaries for some parts (ringrtc) are starting to work; native compilation still isn't there yet, though.

**Note:** As of signal-desktop 5.1.0, I provide two different pre-built ringrtc-node-binaries in my forked repo – one is ABI compatible to the current signal-desktop dependencies, while the other one was built from the ringrtc HEAD (providing ringtc-node v2.10.0 at the time of me writing this note.)

**Note:** ~~Can optionally be built without providing `fpm` and its dependencies as packages by using the `PKGBUILD_fpm_as_gem` – this is a somewhat ugly solution which might clutter your system when just runn via `makepkg` – I'd recommend using a [clean chroot](https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_clean_chroot) or a dockerized environment.~~

**Note:** The whole `fpm` mess in now handled by slightly patching the `package.json` to stop building a `.deb` at all – combined with `--dir` this just doesn't need `fpm` anymore.

**Note:** As of version 1.35.1, Signal includes voice and video calling via ringrtc.
Ringrtc comes as a prebuilt `libringrtc.node` file of the node dependency, which (to probably no-one's surprise) does not come `aarch64`-flavored – there's only a "linux"-build, which is for `x86_64`.

**Note:** ~~As of 1.40.0, things have become even more messy. We now have dependencies that need to be built for aarch64 depending on non-existing upstream commits, or depending on not-yet-updated versions of rust dependencies which break things on aarch64 – and using fpm broke completely for me. Thus, I've had to remove the deb-part completely, and one dependency is currently just used prebuilt from a previous run (when the commit was still there), as I just couldn't be bothered anymore. On the upside, things might now work without any fpm-shenanigans. On another upside, there seem to be efforts going on upstream to cross-compile for more arches (might have to thank the M1 for that?) as well as to move to electron 12 (which would include native Wayland support) – so maybe, just maaaybe I can drop all this soon!~~
Things are getting better. Slowly.

As the build process for ringrtc is pretty convoluted and could probably not reasonably be done in the PKGBUILD, I've provided the required file in a [fork](https://github.com/lsfxz/signal-ringrtc-node/tree/aarch64) (aarch64-branch) of the official signal-ringrtc-node-repo.

If you want to build this dependency yourself as well, you can use a [fork](https://github.com/lsfxz/ringrtc/tree/aarch64) of the ringrtc-repo (aarch64-branch), where I've just slightly modified build scripts to build it yourself.

The steps required to do this on Manjaro on an aarch64-system are outlined below:

```bash
# fork of signalapp/ringrtc with some slight modifications to the build scripts
git clone https://github.com/lsfxz/ringrtc
cd ringrtc
git checkout aarch64

# the arch/Manjaro depot tools do not seem to agree with the build scripts
git clone https://chromium.googlesource.com/chromium/tools/depot_tools ../depot_tools
cd ..
export PATH="$(pwd)/depot_tools:${PATH}"

# something new that broke: depot tools. workaround:
export DEPOT_TOOOLS_BOOTSTAP_PYTHON3=0

# gn-m87 might not be required anymore with the most recent ringrtc.
# haven't tested this yet, though.
paru -S --needed rustup ninja yarn gn-m87 clang

rustup toolchain install 1.51.0
rustup default 1.51.0

cd ringrtc
make distclean
make electron PLATFORM=unix NODEJS_ARCH=arm64


# if everything went well, the aarch64-binary is now available at ./src/node/build/linux/libringrtc.node
```

## standardnotes-desktop{,-git}

Pretty much just builds from the [AUR package](https://aur.archlinux.org/packages/standardnotes-desktop/).

Included mostly because it's part of the binary-repo.

git-version made available to build it from the latest git commit.

**Note:** Currently quite broken / un-buildable on `aarch64`. Because FU node-sass-gyp-whatever-`-m64`-flag-cmdline-issues.

## tar-multi

This is just tar with multi-threaded backends used per default. Not an aarch64-specific-thing, just useful imho.

The dirrem.patch is just an ugly hack used to avoid build/test failures when building in a dockerized CI pipeline.

Based on [upstream core/tar](https://www.archlinux.org/packages/core/x86_64/tar/).

## teams-for-linux

Fixed some issues with the AUR PKGBUILD to make it build at all (using nvm) and to get it to package properly on `aarch64`.

## uhk-agent

The [agent](https://github.com/UltimateHackingKeyboard/agent) application for the Ultimate Hacking Keyboard is only available as a [prebuilt AppImage](https://aur.archlinux.org/packages/uhk-agent-appimage/) in the AUR.

This builds it from source and runs it directly via electron.

**Note:** This builds from the latest git commits for a not explicitely supported architecture and should thus be considered quite experimental!

Before using it to update your firmware or something dangerous like it, make sure you know what you're doing and know how to unbrick a potentially bricked Ultimate Hacking Keyboard!

## zram-swap

Made for https://forum.pine64.org/showthread.php?tid=8812.
